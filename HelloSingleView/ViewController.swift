//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Daniel Ekeroth on 2019-10-22.
//  Copyright © 2019 Daniel Ekeroth. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonAction(_ sender: Any) {
        print("hello button")
    }
    
}

